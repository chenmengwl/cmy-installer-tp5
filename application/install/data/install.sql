-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2022-11-20 23:32:59
-- 服务器版本： 5.6.50-log
-- PHP 版本： 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `h.haoltw.com`
--

-- --------------------------------------------------------

--
-- 表的结构 `box_account`
--

CREATE TABLE `box_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` int(10) UNSIGNED DEFAULT NULL,
  `device_id` int(10) UNSIGNED DEFAULT NULL,
  `account` varchar(255) DEFAULT '',
  `status` int(10) UNSIGNED DEFAULT '1',
  `remark` varchar(255) DEFAULT '',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_ad`
--

CREATE TABLE `box_ad` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `box_ad`
--

INSERT INTO `box_ad` (`id`, `title`, `content`, `sort`, `add_time`, `status`) VALUES
(5, '百度搜 红手指云手机 支持安卓 苹果 电脑PC,进行挂APP监控,7X24小时在线!', '<p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">红手指云手机，在手机的APP市场都可以下载使用，进行挂APP监控,7X24小时在线稳定性强!平台推荐:</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">云手机，挂监控在云手机里设置好挂好监控可以关闭云手机的进程了，就是和手机无关了，云<br style=\"margin: 0px; padding: 0px;\"/></span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">端手机自己有网络，也不用电，推荐一款便宜的，百度搜索，红手指云手机，安卓，苹果，PC，都可以挂</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"margin: 0px; padding: 0px; color: rgb(255, 0, 0);\">官方下载</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">https://www.gc.com.cn/</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">使用教程很简单</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">第一步：下载安卓或是苹果版本云手机安装到手机</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">第二步：在云手机中安装监控APP，微信或者支付宝</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">第三步：配置好监控APP并监测心跳与回调通知成功</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">第四步：登录收款使用的微信与支付宝。登录店员号监听也可以</span></strong></p><p microsoft=\"\" helvetica=\"\" -webkit-tap-highlight-color:=\"\" font-size:=\"\" white-space:=\"\" background-color:=\"\" style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px;\"><strong><span style=\"color: rgb(255, 0, 0);\">第五步：享受7X24小时稳定在线的效果吧！！</span></strong></p><p><strong><span style=\"color: rgb(255, 0, 0);\"><br/></span></strong></p><p><br/></p>', 1, 1632118495, 0),
(6, '免挂使用说明,点添加传收款码点获取cookie扫码登录自动获取CK10秒后刷新', '<p><strong style=\"white-space: normal;\"><span style=\"margin: 0px; padding: 0px; color: rgb(255, 0, 0);\">免挂使用说明,点添加传收款码,点获取cookie扫码登录自动获取CK后,点添加10秒后刷新看CK状态正常就可以了,支付宝关闭自动转到余额宝-QQ忽略支付宝设置</span></strong></p>', 0, 1632118515, 0),
(7, '微信PC监控端配置教程,安装配套微信版本,登录微信后点左下角设置关闭更新', '<p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">安装配套微信版本，登录微信后点左下角设置，关闭自动更新</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">打开监控软件</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">点击软件上的，应用，那项，右键有个，启动，</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">然后右键，设置那，填写配置数据，在平台的商户配置项，监控端那，配置数据那项，全部复制到监控</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">端点×就可以了</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">然后点击监控软件的，账号，选项，点击添加微信，扫描登录就可以了</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">如果在商户中心测试支付没回调，就看看微信号，是否关注，微信收款助手。如果没关注就关注下</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">在测试</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">测试成功就可以对接你自己的，网站或者游戏，什么的了</span></strong></p><p><strong><span style=\"color: rgb(255, 0, 0);\"><br/></span></strong></p><p><br/></p>', 0, 1632118537, 0),
(8, '微信免挂说明,微信昵称如何修改,举个例字,比如你微信名字叫,蔚蓝天空-8888', '<p style=\"white-space: normal;\"><strong><span style=\"margin: 0px; padding: 0px; color: rgb(255, 0, 0);\">开启后请添加 llss66688888 微信号为店员</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><strong><span style=\"color: rgb(255, 0, 0);\">修改自己店主微信的微信昵称为,<span style=\"margin: 0px; padding: 0px;\">商户ID</span>,或者</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><strong><span style=\"color: rgb(255, 0, 0);\"><span style=\"margin: 0px; padding: 0px;\">微信昵称-加商户ID注意要那个横杠</span>,两种方式</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><strong><span style=\"color: rgb(255, 0, 0);\">随意选择之后，微信昵称如何修改举个例字</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><strong><span style=\"color: rgb(255, 0, 0);\">比如你微信名字叫，蔚蓝天空-8888这样后面</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><strong><span style=\"color: rgb(255, 0, 0);\">加-注意要这个杠，加上你的ID就可以了，</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; white-space: normal; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><strong><span style=\"color: rgb(255, 0, 0);\">或者直接修改你微信的昵称为你ID号也可以</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"color: rgb(255, 0, 0);\">联系站长审核</span></strong></p><p><strong><span style=\"color: rgb(255, 0, 0);\"><br/></span></strong></p><p><br/></p>', 0, 1632118556, 0),
(9, '手机APP监控,自己挂店员的小技巧,不用登录多个微信,就可以多个收款码收款', '<p><strong style=\"white-space: normal;\"><span style=\"color: rgb(255, 0, 0);\">手机APP监控,自己挂店员的小技巧,不用登录多个微信就可以多个收款码收款，比如你在，红手指云手机，里挂了一个微信号收款，那么想多个微信收款，不需要挂好几个微信号，只需要，把你其它收款的微信，邀请你在，红手指云手机里的微信，为你店员，就可以了，在传你要收款的微信收款码，就行了，方便多号收款。</span></strong></p>', 0, 1632192393, 0),
(10, '免挂支付宝和QQ钱包,CK失效后会邮箱通知,然后会禁用,重新获取CK启用就行', '<p><strong style=\"color: rgb(255, 0, 0); white-space: normal;\">免挂支付宝和QQ钱包,CK失效后会邮箱通知,为了避免受到买家投诉，然后会禁用,重新获取CK后启用就可以</strong></p>', 0, 1639794126, 0);

-- --------------------------------------------------------

--
-- 表的结构 `box_admin`
--

CREATE TABLE `box_admin` (
  `id` smallint(5) UNSIGNED NOT NULL COMMENT '用户id',
  `names` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `email` varchar(200) DEFAULT '' COMMENT 'email',
  `password` varchar(200) DEFAULT '' COMMENT '密码',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `last_login` varchar(50) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(50) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `phone` varchar(14) DEFAULT NULL,
  `role_id` varchar(50) NOT NULL DEFAULT '0' COMMENT '角色id',
  `status` tinyint(10) NOT NULL DEFAULT '1' COMMENT '状态 1正常，2冻结',
  `num` int(11) DEFAULT NULL COMMENT '登录的次数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `box_admin`
--

INSERT INTO `box_admin` (`id`, `names`, `email`, `password`, `add_time`, `last_login`, `last_ip`, `phone`, `role_id`, `status`, `num`) VALUES
(1, 'admin', '70002@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 1640002277, '2022-01-24 20:49:22', '218.12.15.100', '15176226337', '1', 1, 1343);

-- --------------------------------------------------------

--
-- 表的结构 `box_auth_menu`
--

CREATE TABLE `box_auth_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单名',
  `parent_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '父级ID, 0为顶级菜单',
  `status` tinyint(4) DEFAULT '1' COMMENT '1表示显示，2表示隐藏',
  `url` varchar(100) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0' COMMENT '排序',
  `type` varchar(10) NOT NULL DEFAULT 'menu' COMMENT '类型 menu表示菜单栏控制，per表示节点控制',
  `ico` varchar(255) DEFAULT NULL COMMENT '图标'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `box_auth_menu`
--

INSERT INTO `box_auth_menu` (`id`, `name`, `parent_id`, `status`, `url`, `sort`, `type`, `ico`) VALUES
(1, '管理员管理', 0, 1, NULL, 2, 'menu', ''),
(2, '收款码管理', 0, 1, NULL, 3, 'menu', '&#xe627;'),
(3, '角色管理', 1, 1, 'admin/admin/role', 0, 'menu', NULL),
(4, '权限管理', 1, 1, 'admin/admin/permission', 0, 'menu', NULL),
(5, '管理员列表', 1, 1, 'admin/admin/index', 0, 'menu', NULL),
(6, '菜单栏管理', 1, 1, 'admin/menu/index', 9, 'menu', ''),
(7, '添加管理员', 5, 1, 'admin/admin/addAdmin', 0, 'per', NULL),
(8, '删除管理员', 5, 1, 'admin/admin/delAdmin', 0, 'per', NULL),
(9, '修改管理员状态', 5, 1, 'admin/admin/updateAdminStatus', 0, 'per', NULL),
(10, '修改管理员', 5, 1, 'admin/admin/editAdmin', 0, 'per', NULL),
(11, '添加角色', 3, 1, 'admin/admin/addRole', 0, 'per', NULL),
(12, '编辑角色', 3, 1, 'admin/admin/editRole', 0, 'per', NULL),
(13, '删除角色', 3, 1, 'admin/admin/delRole', 0, 'per', NULL),
(14, '添加权限', 4, 1, 'admin/admin/addPermission', 0, 'per', NULL),
(15, '编辑权限', 4, 1, 'admin/admin/editPermission', 0, 'per', NULL),
(16, '删除权限', 4, 1, 'admin/admin/delPermission', 0, 'per', NULL),
(17, '添加菜单栏', 6, 1, 'admin/menu/addMenu', 0, 'per', NULL),
(18, '修改菜单栏', 6, 1, 'admin/menu/editMenu', 0, 'per', NULL),
(20, '删除菜单栏', 6, 1, 'admin/menu/delMenu', 1, 'per', NULL),
(26, '系统管理', 0, 1, NULL, 0, 'menu', '&#xe63c;'),
(32, '日志列表', 55, 1, 'admin/system/operation', 0, 'menu', ''),
(34, '删除所有', 32, 1, 'admin/system/delOperation', 0, 'per', NULL),
(35, '清空缓存', 32, 1, 'admin/base/clear', 0, 'per', NULL),
(36, '商户管理', 0, 1, NULL, 8, 'menu', ''),
(37, '商户列表', 36, 1, 'admin/users/index', 0, 'menu', ''),
(39, '会员的添加', 37, 1, 'admin/users/addUser', 0, 'per', NULL),
(40, '会员的编辑', 37, 1, 'admin/users/editUser', 0, 'per', NULL),
(41, '会员的删除', 37, 1, 'admin/users/delUser', 0, 'per', NULL),
(42, '添加公告', 56, 1, 'admin/manage/addAd', 0, 'per', NULL),
(43, '修改会员状态', 37, 1, 'admin/users/updateUserStatus', 0, 'per', NULL),
(44, '导出会员信息', 37, 1, 'admin/users/userExcel', 0, 'per', NULL),
(47, '收款码列表', 2, 1, 'admin/qrcodes/index', 0, 'menu', ''),
(48, '基础配置', 26, 1, 'admin/system/base', 0, 'menu', ''),
(49, '删除', 48, 1, 'admin/system/del', 0, 'per', NULL),
(50, '更新状态', 48, 1, 'admin/system/update', 0, 'per', NULL),
(51, '订单管理', 0, 1, NULL, 6, 'menu', '&#xe628;'),
(52, '支付订单', 51, 1, 'admin/orders/pay?type=1', 6, 'menu', ''),
(53, '升级订单', 51, 1, 'admin/orders/pay?type=2', 8, 'menu', ''),
(54, '商户订单', 51, 1, 'admin/orders/index', 0, 'menu', ''),
(55, '网站功能', 0, 1, NULL, 9, 'menu', '&#xe72b;'),
(56, '会员公告', 55, 1, 'admin/manage/ad', 0, 'menu', ''),
(58, '平台订单', 52, 1, 'admin/orders/pay', 0, 'per', NULL),
(62, '编辑公告', 56, 1, 'admin/manage/editAd', 0, 'per', NULL),
(66, '权限批量删除', 4, 1, 'admin/admin/delPermissionAll', 0, 'per', NULL),
(67, '菜单批量删除', 6, 1, 'admin/menu/delMenuAll', 0, 'per', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `box_auth_role`
--

CREATE TABLE `box_auth_role` (
  `role_id` tinyint(3) UNSIGNED NOT NULL COMMENT '自增ID',
  `role_name` varchar(100) NOT NULL DEFAULT '' COMMENT '角色名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `menu_id` varchar(255) NOT NULL COMMENT '菜单栏id',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色表';

--
-- 转存表中的数据 `box_auth_role`
--

INSERT INTO `box_auth_role` (`role_id`, `role_name`, `desc`, `menu_id`, `modified`) VALUES
(1, '超级管理员', '超级管理员', '26,32,34,35,48,49,50,1,3,11,12,13,4,14,15,16,66,5,7,8,9,10,6,17,18,20,67,2,47,51,54,52,58,53,36,37,39,40,41,43,44,64,55,56,42,62,38', '2020-07-04 10:05:36');

-- --------------------------------------------------------

--
-- 表的结构 `box_config`
--

CREATE TABLE `box_config` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `value` text,
  `remark` varchar(100) DEFAULT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'site',
  `status` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `box_config`
--

INSERT INTO `box_config` (`id`, `code`, `value`, `remark`, `type`, `status`) VALUES
(1, 'site_name', '码支付', '网站名称', 'site', 0),
(2, 'site_title', '免签约充值接口对接平台,支付宝免签约即时到账,财付通免签约,微信免签约支付,QQ钱包免签约,免签约支付,免签约易支付', '网站标题', 'site', 0),
(3, 'site_desc', '免签支付系统', '网站简介', 'site', 0),
(4, 'site_keywords', '码支付,免签约充值接口对接平台,支付宝免签约即时到账,财付通免签约,微信免签约支付,QQ钱包免签约,免签约支付', '网站关键词', 'site', 0),
(5, 'site_description', '码支付是网络科技有限公司旗下的免签约支付产品，完美解决支付难题，一站式接入支付宝，微信，财付通，QQ钱包,微信wap，帮助开发者快速集成到自己相应产品，效率高，见效快，费率低！', '站点描述', 'site', 0),
(6, 'site_author', 'Pay', '站点作者', 'site', 0),
(7, 'site_url', 'https://mzf.qqq.com', '站点链接', 'site', 0),
(8, 'site_logo', NULL, '站点LOGO', 'site', 0),
(9, 'site_engname', 'longtengmazhifu', '英文简称', 'site', 0),
(10, 'site_wechat', '635188888', '客服微信', 'site', 0),
(11, 'site_qq', '88888888', '客服QQ', 'site', 0),
(12, 'site_mail', '88888888@qq.com', '客服邮箱', 'site', 0),
(13, 'site_icp', '贵ICP备202258687号-8', '站点备案号', 'site', 0),
(14, 'site_api', 'mzf.qqq.com', 'API接口域名', 'site', 0),
(15, 'site_app', 'https://mzf.qqq.com/htdocs/budpay.apk', '监控端下载链接', 'site', 0),
(16, 'site_mid', '999', '初始商户编号', 'site', 0),
(17, 'site_ff', NULL, '分发平台链接', 'site', 0),
(18, 'user_level_mout', '3', '注册会员费率', 'user', 0),
(19, 'user_level1_mout', '3', '基础版费率', 'user', 0),
(20, 'user_level2_mout', '2', '标准版费率', 'user', 0),
(21, 'user_level3_mout', '1', '高级版费率', 'user', 0),
(22, 'user_level1_price', '0', '基础版升级价格', 'user', 0),
(23, 'user_level2_price', '20', '标准版升级价格', 'user', 0),
(24, 'user_level3_price', '30', '高级版升级价格', 'user', 0),
(25, 'reg_give_money', '5', '注册赠送金额', 'user', 0),
(26, 'reg_give_level', '0', '注册赠送体验套餐', 'reg', 0),
(27, 'reg_give_day', '0', '注册赠送体验天数', 'reg', 0),
(28, 'email_user1', '88888888@qq.com', '邮箱提醒账号1用户名', 'email', 0),
(29, 'email_password1', 'xtgnaiabarcxdhab', '邮箱提醒账号1密码', 'email', 0),
(30, 'email_smtp1', 'smtp.qq.com', '邮箱系统账号1SMTP', 'email', 0),
(31, 'email_port1', '465', '邮箱系统账号1SMTP端口', 'email', 0),
(32, 'email_user2', '88888888@qq.com', '邮箱提醒账号2用户名', 'email', 0),
(33, 'email_password2', 'xtgnaiabarcxdhab', '邮箱提醒账号2密码', 'email', 0),
(34, 'email_smtp2', 'smtp.qq.com', '有限提醒账号2SMTP', 'email', 0),
(35, 'email_port2', '465', '邮箱提醒账号2SMTP端口', 'email', 0),
(36, 'pay_mode', '1', '平台收款模式0全部1平台收款2官方接口', 'pay', 0),
(37, 'pay_qrcode_mid', '1012', '系统收款使用商户ID', 'pay', 0),
(38, 'pay_qrcode_mode', '0', '0全部接口1微信2支付宝', 'pay', 0),
(39, 'admin_login_path', 'admin', '后台登陆路径', 'site', 0),
(40, 'appid', '', '应用APPID', 'site', 0),
(41, 'appkey', '', '支付宝公钥(RSA2)', 'site', 0),
(42, 'appsecret', '', '商户私钥(RSA2)', 'site', 0),
(43, 'user_level_fencheng', '20', '注册会员分成比例', 'user', 0),
(47, 'sms_id', 'LTAI5tRvy3BMHXVQXZSafGVu', 'AccessKey ID', 'sms', 0),
(48, 'sms_secret', 'PDKaZ1Ls2zjoGLXXGRrwyzRofLrikf', 'AccessKey Secret', 'sms', 0),
(49, 'sms_temp', 'SMS_2181888013', '模板', 'sms', 0),
(50, 'sms_sign', '码支付', '签名', 'sms', 0),
(51, 'sms_open', '0', '是否启用短信验证', 'sms', 0),
(52, 'sms_id_ck', '', 'AccessKey ID', 'sms', 0),
(53, 'sms_secret_ck', '', 'AccessKey Secret', 'sms', 0),
(54, 'sms_temp_ck', '', '模板', 'sms', 0),
(56, 'sms_sign_ck', '', '签名', 'sms', 0),
(57, 'sms_open_ck', '1', '是否启用短信验证', 'sms', 0),
(58, 'sms_ck_money', '0', '扣费', 'sms', 0),
(59, 'site_money', '3', '余额提示', 'site', 0),
(60, 'pay_url_1', 'mzf.qqq.com', '支付域名1', 'site', 0),
(61, 'pay_url_2', '', '支付域名2', 'site', 0),
(62, 'pay_url_3', '', '支付域名3', 'site', 0),
(63, 'api_url_1', 'mzf.qqq.com', 'API域名1', 'site', 0),
(64, 'api_url_2', 'mzf.qqq.com', 'API域名2', 'site', 0),
(65, 'api_url_3', 'mzf.qqq.com', 'API域名3', 'site', 0),
(66, 'cron_wx_dy', '', '店员IP', 'site', 0);

-- --------------------------------------------------------

--
-- 表的结构 `box_email_code`
--

CREATE TABLE `box_email_code` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'id',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'email',
  `code` char(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1234' COMMENT 'code',
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ip地址',
  `createtime` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
  `subject_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '验证码类型'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `box_fencheng`
--

CREATE TABLE `box_fencheng` (
  `id` int(10) UNSIGNED NOT NULL,
  `xjmid` int(10) UNSIGNED DEFAULT NULL COMMENT '下级商户ID',
  `oid` varchar(200) DEFAULT '' COMMENT '平台云端订单号',
  `type` varchar(200) DEFAULT '' COMMENT '类型',
  `price` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT '充值金额',
  `money` decimal(10,2) DEFAULT NULL COMMENT '分成金额',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `ctime` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_log`
--

CREATE TABLE `box_log` (
  `id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `content` varchar(200) NOT NULL,
  `ctime` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_operation`
--

CREATE TABLE `box_operation` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL COMMENT '操作管理员',
  `action` varchar(255) DEFAULT NULL COMMENT '操作方法',
  `time` int(11) DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `box_pay`
--

CREATE TABLE `box_pay` (
  `id` int(11) NOT NULL,
  `sn` varchar(100) NOT NULL COMMENT '订单编号',
  `mid` int(11) NOT NULL COMMENT '商户ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `level` tinyint(2) DEFAULT '0' COMMENT '升级等级',
  `day` int(11) DEFAULT NULL COMMENT '升级天数',
  `ctime` int(11) NOT NULL COMMENT '创建时间',
  `ptime` int(11) DEFAULT NULL COMMENT '支付时间',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态1待支付 0支付 2超时',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1充值 2套餐',
  `mode` tinyint(2) NOT NULL DEFAULT '1' COMMENT '支付模式1平台商户 2官方接口',
  `zhifu` int(11) NOT NULL DEFAULT '1' COMMENT '1:微信 2:支付宝'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_pay_dmf`
--

CREATE TABLE `box_pay_dmf` (
  `id` int(10) NOT NULL,
  `uid` int(11) NOT NULL,
  `title` varchar(60) NOT NULL COMMENT '标题',
  `ali_pubkey` text NOT NULL COMMENT '支付宝公钥',
  `skey` text NOT NULL COMMENT '商户私钥',
  `appid` char(30) NOT NULL COMMENT '应用ID',
  `add_time` int(11) NOT NULL,
  `suc_num` int(11) NOT NULL DEFAULT '0' COMMENT '收款笔数',
  `suc_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '收款总金额',
  `all_num` int(11) NOT NULL DEFAULT '0' COMMENT '发起单量',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1开启 0关闭'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_pay_noorder`
--

CREATE TABLE `box_pay_noorder` (
  `id` bigint(20) NOT NULL,
  `mid` int(11) NOT NULL,
  `qid` int(11) DEFAULT NULL,
  `close_date` bigint(20) NOT NULL,
  `create_date` bigint(20) NOT NULL,
  `is_auto` int(11) NOT NULL,
  `notify_url` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `param` varchar(255) DEFAULT NULL,
  `pay_date` bigint(20) NOT NULL,
  `pay_id` varchar(255) DEFAULT NULL,
  `pay_url` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `really_price` double NOT NULL,
  `return_url` varchar(255) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_pay_order`
--

CREATE TABLE `box_pay_order` (
  `id` bigint(20) NOT NULL,
  `mid` int(11) NOT NULL,
  `qid` int(11) DEFAULT NULL,
  `close_date` bigint(20) NOT NULL,
  `create_date` bigint(20) NOT NULL,
  `is_auto` int(11) NOT NULL,
  `notify_url` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `param` varchar(255) DEFAULT NULL,
  `pay_date` bigint(20) NOT NULL,
  `pay_id` varchar(255) DEFAULT NULL,
  `pay_url` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `really_price` double NOT NULL,
  `return_url` varchar(255) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `last_fee` decimal(10,2) DEFAULT '0.00' COMMENT '剩余手续费额度',
  `is_epay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 易支付方式发起',
  `is_dmf` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1当面付',
  `before_alipay_balance` decimal(10,2) DEFAULT '0.00' COMMENT '支付前余额',
  `before_qq_balance` decimal(10,2) DEFAULT '0.00',
  `huidiao` int(11) NOT NULL DEFAULT '1' COMMENT '回调功能',
  `fee_flag` int(10) UNSIGNED DEFAULT '0' COMMENT '1:已扣除手续费'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `box_pay_qrcode`
--

CREATE TABLE `box_pay_qrcode` (
  `id` bigint(20) NOT NULL,
  `uid` int(11) NOT NULL,
  `qrcode` varchar(255) DEFAULT NULL,
  `pay_url` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1微信 2支付宝',
  `mode` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1无固定 2固定金额',
  `max_num` int(11) NOT NULL DEFAULT '0',
  `max_money` double NOT NULL DEFAULT '0',
  `add_time` int(11) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `cookie` text,
  `ck_status` int(10) DEFAULT '0',
  `last_ck_time` datetime DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT '0.00',
  `account_id` int(10) UNSIGNED DEFAULT '0',
  `alipay_uid` varchar(100) DEFAULT '',
  `lx` int(10) NOT NULL DEFAULT '1',
  `sms_cishu` int(10) NOT NULL DEFAULT '0' COMMENT '短信当前次数',
  `open_pay_qrcode` int(2) NOT NULL DEFAULT '1' COMMENT '是否转换二维码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `box_pay_record`
--

CREATE TABLE `box_pay_record` (
  `id` int(11) NOT NULL,
  `mid` int(11) NOT NULL COMMENT '商户ID',
  `qid` int(11) NOT NULL COMMENT '二维码ID',
  `date` int(11) NOT NULL COMMENT '日期',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1微信 2支付宝',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '收入金额'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_record`
--

CREATE TABLE `box_record` (
  `id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `oid` varchar(200) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `money` decimal(10,2) NOT NULL,
  `ctime` int(11) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT '-',
  `remark` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_tmp_price`
--

CREATE TABLE `box_tmp_price` (
  `price` varchar(255) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `mid` int(11) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `box_user`
--

CREATE TABLE `box_user` (
  `id` int(11) NOT NULL,
  `mid` int(11) DEFAULT NULL COMMENT '商户ID',
  `level` tinyint(2) NOT NULL DEFAULT '0' COMMENT '商户等级0默认',
  `real_name` varchar(64) DEFAULT NULL COMMENT '会员的名称',
  `password` varchar(120) DEFAULT NULL COMMENT '会员密码',
  `cronip` varchar(9999) DEFAULT NULL COMMENT '监控IP',
  `email` varchar(255) DEFAULT NULL COMMENT '会员的邮箱',
  `tel` varchar(120) DEFAULT NULL COMMENT '电话',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '余额',
  `mout` decimal(5,2) DEFAULT '0.50',
  `apikey` varchar(100) DEFAULT NULL COMMENT 'apikey',
  `user_logo` varchar(255) DEFAULT NULL COMMENT '会员头像',
  `last_ip` varchar(255) DEFAULT NULL COMMENT '用户登录的ip',
  `last_time` int(11) DEFAULT NULL COMMENT '会员登录的时间',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新的时间',
  `exp_time` int(11) DEFAULT NULL COMMENT '会员到期时间',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '会员状态 0 禁用 1 启用',
  `popup` int(10) DEFAULT '0',
  `popupmsg` text,
  `rective` tinyint(2) DEFAULT '1' COMMENT '会员删除   0 删除  1恢复',
  `user_browser` varchar(255) DEFAULT NULL COMMENT '用户使用的设备',
  `user_system` varchar(255) DEFAULT NULL COMMENT '系统',
  `close_time` varchar(20) NOT NULL DEFAULT '5' COMMENT '订单关闭时间',
  `returnUrl` varchar(100) DEFAULT NULL,
  `notifyUrl` varchar(100) DEFAULT NULL,
  `payQf` tinyint(2) NOT NULL DEFAULT '1',
  `miangua` tinyint(2) NOT NULL DEFAULT '1' COMMENT '免挂状态 1关闭2开启',
  `lastheart` int(11) DEFAULT NULL,
  `lastpay` int(11) DEFAULT NULL,
  `jkstate` varchar(20) NOT NULL DEFAULT '-1',
  `inviter` int(11) DEFAULT NULL,
  `pmid` int(10) UNSIGNED DEFAULT '0' COMMENT '上级mid',
  `open_sms` int(11) DEFAULT '0' COMMENT '是否开启短信验证',
  `change_mobile` int(11) DEFAULT '0' COMMENT '是否修改过手机',
  `sms_ck_money_open` int(11) DEFAULT '0' COMMENT 'ck和余额短信提醒:0关闭;1开启;',
  `sms_money_count` int(11) DEFAULT '0' COMMENT '余额每天提醒次数',
  `sms_ck_count` int(11) DEFAULT '0' COMMENT 'ck每天提醒次数',
  `sms_line_time` int(11) DEFAULT '1' COMMENT '短信发送默认间隔(小时)',
  `sms_open_ck` int(11) NOT NULL DEFAULT '0' COMMENT '短信ck提示是否开启 1=开启',
  `sms_shu_ck` int(11) NOT NULL DEFAULT '1' COMMENT '短信当天提醒次数 最低1 最高5',
  `tz_time` int(11) NOT NULL DEFAULT '0' COMMENT '余额不足通知'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `box_user`
--

INSERT INTO `box_user` (`id`, `mid`, `level`, `real_name`, `password`, `cronip`, `email`, `tel`, `money`, `mout`, `apikey`, `user_logo`, `last_ip`, `last_time`, `add_time`, `update_time`, `exp_time`, `status`, `popup`, `popupmsg`, `rective`, `user_browser`, `user_system`, `close_time`, `returnUrl`, `notifyUrl`, `payQf`, `miangua`, `lastheart`, `lastpay`, `jkstate`, `inviter`, `pmid`, `open_sms`, `change_mobile`, `sms_ck_money_open`, `sms_money_count`, `sms_ck_count`, `sms_line_time`, `sms_open_ck`, `sms_shu_ck`, `tz_time`) VALUES
(13, 1012, 3, '卡', 'e7f407f9975ad7b6ed21340ea87990f9', NULL, '70002@qq.com', '18888888888', '1180.49', '1.00', '90biqb8l4ky8t130s492g0on4ke5', 'bb', '123.139.81.2', 1653995034, 1585785898, 1629540210, 2147443200, 0, 0, NULL, 1, 'AppleWebKit', 'Pc', '5', '', '', 1, 1, 1642671455, 1642671455, '0', NULL, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0);

--
-- 转储表的索引
--

--
-- 表的索引 `box_account`
--
ALTER TABLE `box_account`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_ad`
--
ALTER TABLE `box_ad`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_admin`
--
ALTER TABLE `box_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_name` (`names`) USING BTREE;

--
-- 表的索引 `box_auth_menu`
--
ALTER TABLE `box_auth_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- 表的索引 `box_auth_role`
--
ALTER TABLE `box_auth_role`
  ADD PRIMARY KEY (`role_id`);

--
-- 表的索引 `box_config`
--
ALTER TABLE `box_config`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_email_code`
--
ALTER TABLE `box_email_code`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_fencheng`
--
ALTER TABLE `box_fencheng`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_log`
--
ALTER TABLE `box_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_operation`
--
ALTER TABLE `box_operation`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_pay`
--
ALTER TABLE `box_pay`
  ADD PRIMARY KEY (`id`),
  ADD KEY ```status``` (`status`) USING BTREE;

--
-- 表的索引 `box_pay_dmf`
--
ALTER TABLE `box_pay_dmf`
  ADD PRIMARY KEY (`id`),
  ADD KEY ```uid``` (`uid`) USING BTREE,
  ADD KEY ```status``` (`status`) USING BTREE;

--
-- 表的索引 `box_pay_noorder`
--
ALTER TABLE `box_pay_noorder`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_pay_order`
--
ALTER TABLE `box_pay_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY ```order_id``` (`order_id`) USING BTREE,
  ADD UNIQUE KEY `pay_id` (`pay_id`),
  ADD KEY ```mid``` (`mid`) USING BTREE,
  ADD KEY ```create_date``` (`create_date`) USING BTREE,
  ADD KEY ```state``` (`state`) USING BTREE,
  ADD KEY `fee_flag` (`fee_flag`);

--
-- 表的索引 `box_pay_qrcode`
--
ALTER TABLE `box_pay_qrcode`
  ADD PRIMARY KEY (`id`),
  ADD KEY ```type``` (`type`) USING BTREE,
  ADD KEY ```uid``` (`uid`) USING BTREE,
  ADD KEY ```mode``` (`mode`) USING BTREE,
  ADD KEY ```status``` (`status`) USING BTREE,
  ADD KEY ```price``` (`price`) USING BTREE,
  ADD KEY ```id``` (`id`) USING BTREE,
  ADD KEY `qrcode` (`qrcode`),
  ADD KEY `pay_url` (`pay_url`),
  ADD KEY `qrcode_2` (`qrcode`),
  ADD KEY `pay_url_2` (`pay_url`),
  ADD KEY `qrcode_3` (`qrcode`),
  ADD KEY `pay_url_3` (`pay_url`);

--
-- 表的索引 `box_pay_record`
--
ALTER TABLE `box_pay_record`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `box_record`
--
ALTER TABLE `box_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oid` (`oid`);

--
-- 表的索引 `box_tmp_price`
--
ALTER TABLE `box_tmp_price`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY ```price``` (`price`) USING BTREE;

--
-- 表的索引 `box_user`
--
ALTER TABLE `box_user`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `box_account`
--
ALTER TABLE `box_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_ad`
--
ALTER TABLE `box_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用表AUTO_INCREMENT `box_admin`
--
ALTER TABLE `box_admin`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `box_auth_menu`
--
ALTER TABLE `box_auth_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- 使用表AUTO_INCREMENT `box_auth_role`
--
ALTER TABLE `box_auth_role`
  MODIFY `role_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增ID', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `box_config`
--
ALTER TABLE `box_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- 使用表AUTO_INCREMENT `box_email_code`
--
ALTER TABLE `box_email_code`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- 使用表AUTO_INCREMENT `box_fencheng`
--
ALTER TABLE `box_fencheng`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_log`
--
ALTER TABLE `box_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_operation`
--
ALTER TABLE `box_operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_pay`
--
ALTER TABLE `box_pay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_pay_dmf`
--
ALTER TABLE `box_pay_dmf`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_pay_noorder`
--
ALTER TABLE `box_pay_noorder`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_pay_order`
--
ALTER TABLE `box_pay_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_pay_qrcode`
--
ALTER TABLE `box_pay_qrcode`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_pay_record`
--
ALTER TABLE `box_pay_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_record`
--
ALTER TABLE `box_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_tmp_price`
--
ALTER TABLE `box_tmp_price`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `box_user`
--
ALTER TABLE `box_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
