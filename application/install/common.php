<?php

//删除目录（递归删除）

function delDir($dir)
{
    //传入文件的路径
    //遍历目录
    $arr = scandir($dir);
    foreach ($arr as $val) {
        if ($val != '.' && $val != '..') {
            //路径链接
            $file = $dir . '/' . $val;
            if (is_dir($file)) {
                delDir($file);
            } else {
                unlink($file);
            }
        }
    }
    rmdir($dir);
}

// 目录复制
function copyDir($dir1, $dir2)
{
    if (!file_exists($dir2)) {
        $cdir = mkdir($dir2);
    }
    //遍历原目录
    $arr = scandir($dir1);
    foreach ($arr as $val) {
        if ($val != '.' && $val != '..') {
            //原目录拼接
            $sfile = $dir1 . '/' . $val;
            //目的目录拼接
            $dfile = $dir2 . '/' . $val;
            if (is_dir($sfile)) {
                copyDir($sfile, $dfile);
            } else {
                copy($sfile, $dfile);
            }
        }
    }
}

// 移动目录
function moveDir($dir1, $dir2)
{
    copyDir($dir1, $dir2);
    delDir($dir1);
}

$allow_method = ['strat_license', 'env_check', 'app_reg', 'db_init', 'ext_info', 'install_check', 'tablepre_check'];
function getgpc($k, $t = 'GP')
{
    $t = strtoupper($t);
    switch ($t) {
        case 'GP':
            isset($_POST[$k]) ? $var =  &$_POST : $var =  &$_GET;
            break;
        case 'G':
            $var =  &$_GET;
            break;
        case 'P':
            $var =  &$_POST;
            break;
        case 'C':
            $var =  &$_COOKIE;
            break;
        case 'R':
            $var =  &$_REQUEST;
            break;
    }
    return isset($var[$k]) ? $var[$k] : null;
}

////判断目录读写权限
function testwrite($d)
{
    if (is_file($d)) {
        if (is_writeable($d)) {
            return true;
        }
        return false;

    } else {
        $tfile = "_test.txt";
        $fp    = @fopen($d . "/" . $tfile, "w");
        if (!$fp) {
            return false;
        }
        fclose($fp);
        $rs = @unlink($d . "/" . $tfile);
        if ($rs) {
            return true;
        }
        return false;
    }

}

function dir_create($path, $mode = 0777)
{
    if (is_dir($path)) {
        return true;
    }

    $ftp_enable = 0;
    $path       = dir_path($path);
    $temp       = explode('/', $path);
    $cur_dir    = '';
    $max        = count($temp) - 1;
    for ($i = 0; $i < $max; $i++) {
        $cur_dir .= $temp[$i] . '/';
        if (@is_dir($cur_dir)) {
            continue;
        }

        @mkdir($cur_dir, 0777, true);
        @chmod($cur_dir, 0777);
    }
    return is_dir($path);
}

function dir_path($path)
{
    $path = str_replace('\\', '/', $path);
    if (substr($path, -1) != '/') {
        $path = $path . '/';
    }

    return $path;
}

function filesize_formatted($path)
{
    $size  = filesize($path);
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

if (!function_exists('file_size_format')) {
    /**
     * 文件大小格式化
     * @param integer $size 初始文件大小，单位为byte
     * @return array 格式化后的文件大小和单位数组，单位为byte、KB、MB、GB、TB
     * @example file_size_format(123456789);
     */
    function file_size_format($size = 0, $dec = 2)
    {
        $unit = array("B", "KB", "MB", "GB", "TB", "PB");
        $pos  = 0;
        while ($size >= 1024) {
            $size /= 1024;
            $pos++;
        }
        $result['size'] = round($size, $dec);
        $result['unit'] = $unit[$pos];
        return $result['size'] . $result['unit'];

    }
}
if (!function_exists('mitobyte')) {
    /**
     * Converts a human readable file size value to a number of bytes that it
     * represents. Supports the following modifiers: K, M, G and T.
     * Invalid input is returned unchanged.
     *
     * Example:
     * <code>
     * $config->human2byte(10);          // 10
     * $config->human2byte('10b');       // 10
     * $config->human2byte('10k');       // 10240
     * $config->human2byte('10K');       // 10240
     * $config->human2byte('10kb');      // 10240
     * $config->human2byte('10Kb');      // 10240
     * // and even
     * $config->human2byte('   10 KB '); // 10240
     * </code>
     *
     * @param number|string $value
     * @return number
     */

    function mitobyte($value)
    {
        return preg_replace_callback('/^\s*(\d+)\s*(?:([kmgt]?)b?)?\s*$/i', function ($m) {
            switch (strtolower($m[2])) {
                case 't':
                    $m[1] *= 1024;
                case 'g':
                    $m[1] *= 1024;
                case 'm':
                    $m[1] *= 1024;
                case 'k':
                    $m[1] *= 1024;
            }
            return $m[1];
        }, $value);
    }
}
/**
 **
 * 检查连接是否可用
 * @param Link $dbconn 数据库连接
 * @return Boolean
 */

function pdo_ping($dbconn)
{
    try {
        $dbconn->getAttribute(PDO::ATTR_SERVER_INFO);
    } catch (PDOException $e) {
        if (strpos($e->getMessage(), 'MySQL server has gone away') !== false) {
            return false;
        }

    }
    return true;

}

/**
 * 删除当前目录及其目录下的所有目录和文件
 * @param string $path 待删除的目录
 * @note  $path路径结尾不要有斜杠/(例如:正确[$path='./static/image'],错误[$path='./static/image/'])
 */
function deleteDir($path)
{

    if (is_dir($path)) {
        //扫描一个目录内的所有目录和文件并返回数组
        $dirs = scandir($path);

        foreach ($dirs as $dir) {
            //排除目录中的当前目录(.)和上一级目录(..)
            if ($dir != '.' && $dir != '..') {
                //如果是目录则递归子目录，继续操作
                $sonDir = $path . '/' . $dir;
                if (is_dir($sonDir)) {
                    //递归删除
                    deleteDir($sonDir);

                    //目录内的子目录和文件删除后删除空目录
                    @rmdir($sonDir);
                } else {

                    //如果是文件直接删除
                    @unlink($sonDir);
                }
            }
        }
        @rmdir($path);
    }
}

if (!function_exists('array_get')) {
    /**
     * 快捷获取数组指定键的成员 可避免php框架的严格模式下报错
     * 如 $arr = ['name'=>'测试','data'=> ['a'=>'555','b'=>'6565']]; 可以 array_get('data.b', $arr);
     * @param  string|number $key     key 支持无限级获取
     * @param  array         $array   数组
     * @param  string        $default 默认值
     */
    function array_get($key = null, $array = [], $default = '')
    {
        if (is_array($array)) {
            if (is_null($key)) {
                $key = '0';
            }
            if (strpos($key, '.') > 0) {
                $temp   = explode('.', $key, 2);
                $key2   = $temp[0];
                $array2 = array_has($key2, $array) ? $array[$key2] : $default;
                if (is_array($array2)) {
                    return array_get($temp[1], $array2);
                }
                return $default;
            }
            return array_has($key, $array) ? $array[$key] : $default;
        }
        return $default;
    }
}

if (!function_exists('array_has')) {
    /**
     * 快捷检测数组指定键是否存在
     * @param  string $key   key
     * @return [type]        数组
     */
    function array_has($key = null, $array = [])
    {
        if (is_array($array) && !is_null($key)) {
            if (strpos($key, '.') > 0) {
                $temp = explode('.', $key, 2);
                return array_has($temp[1], $array[$temp[0]]);
            }
            is_numeric($key) && $key = intval($key);
            return array_key_exists($key, $array);
        }
        return false;
    }
}