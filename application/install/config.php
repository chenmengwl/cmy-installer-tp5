<?php
// +----------------------------------------------------------------------
// | 安装配置
// +----------------------------------------------------------------------
return [
    'install' => [
        // 配置信息
        'config'  => [
            // 每次执行语句条数
            'write_num'    => 20,

            // 当前数据文件表前缀
            'table_prefix' => ['box_'],

            // 当前数据文件表前缀
            'doc_url'      => '<a href="https://doc.pro.cmyo.cn/">https://doc.pro.cmyo.cn/</a>',
        ],

        // 写入选项
        'write'   => [
            // 站点信息
            'site'    => true,
            // 配置信息 install目录下需有 version配置文件
            // 写入字段信息请参考控制器insertVersionInfo方法
            'version' => true,
        ],

        // 加密选项
        'encrypt' => [
            // 类型 可选 函数 function 或 类 class
            'type'   => 'function',
            // 驱动 类型是函数时就是函数名称、是类时就指定类路径
            'drive'  => 'pswCrypt',
            // 可选 驱动为类时使用
            'method' => null,
        ],

        'model'   => [
            // 管理员表
            'admin'  => [
                // 表名
                'name'     => 'admin',
                // 模型类 留空时可表名不能为空
                'model'    => \app\admin\model\AdminModel::class,
                // 主键字段
                'keyField' => 'id',
            ],
            // 配置表
            'config' => [
                'name'      => 'config',
                'model'     => null,
                'keyField'  => 'id',
                // 配置名字段
                'nameField' => 'code',
                // 配置值字段
                'valueId'   => 'value',
            ],
        ],

        // 安装表单列表 有多个表单组成 如 数据库信息、网站配置等，可自由修改
        // 注意 如果生成的表单有异常，请检查定义的字段名是否存在重复
        'form'    => [
            [
                // 表单类别名称
                'name'  => '填写数据库配置',
                // 表单数据集储存字段
                'field' => 'database',
                'items' => [
                    [
                        // 标题
                        'label'       => '服务器地址',
                        // 字段
                        'field'       => 'dbhost',
                        // 标题长度 支持string和number
                        'label-width' => 145,
                        // 提示
                        'tips'        => '一般默认127.0.0.1, 如不在同服务器上请查看数据库控制台',
                        // 字段类型 只支持number 数字、text 普通文本、password密码文本三种
                        'type'        => 'text',
                        // 默认值
                        'default'     => '127.0.0.1',
                        // 是否必填
                        'required'    => true,
                    ],
                    [
                        'label'       => '数据库端口',
                        'field'       => 'dbport',
                        'label-width' => 145,
                        'type'        => 'number',
                        'tips'        => '一般服务器都是默认3306, 如无法确认请咨询数据库提供商',
                        'default'     => 3306,
                        'required'    => true,
                    ],
                    [
                        'label'       => '数据库名称',
                        'field'       => 'dbname',
                        'label-width' => 145,
                        'tips'        => '在宝塔上是和用户名一样的',
                        'type'        => 'text',
                        'default'     => '',
                        'required'    => true,
                    ],
                    [
                        'label'       => '数据库账号',
                        'field'       => 'dbuser',
                        'label-width' => 145,
                        'tips'        => '',
                        'type'        => 'text',
                        'default'     => '',
                        'required'    => true,
                    ],
                    [
                        'label'       => '数据库密码',
                        'field'       => 'dbpwd',
                        'label-width' => 145,
                        'tips'        => '',
                        'type'        => 'password',
                        'default'     => '',
                        'required'    => true,
                    ],
                    [
                        'label'       => '数据库表前缀',
                        'field'       => 'dbprefix',
                        'label-width' => 145,
                        'tips'        => '建议使用默认, 同一数据库安装多个程序时需修改',
                        'type'        => 'text',
                        'default'     => 'cmzf_',
                        'required'    => true,
                    ],

                ],
            ],
            [
                'name'  => '填写网站配置',
                'field' => 'website',
                'items' => [
                    [
                        'label'       => '站点名称',
                        'field'       => 'sitename',
                        'label-width' => 145,
                        'type'        => 'text',
                        'tips'        => '',
                        'default'     => '沉梦码支付',
                        'required'    => true,
                    ],
                    [
                        'label'       => '管理员用户名',
                        'field'       => 'names',
                        'label-width' => 145,
                        'type'        => 'text',
                        'tips'        => '长度在6~20之间(含20), 仅支持大小字母、数字、小数点、@符号组合',
                        'default'     => '',
                        'required'    => true,
                    ],
                    [
                        'label'       => '管理员密码',
                        'field'       => 'password',
                        'label-width' => 145,
                        'type'        => 'text',
                        'tips'        => '长度在8~32之间(含32), 仅支持大小字母、数字、小数点、@符号组合',
                        // 'default'     => Random::alnum(10),
                        'default'     => '',
                        'required'    => true,
                    ],
                    [
                        'label'       => '重复管理员密码',
                        'field'       => 'password_repeat',
                        'label-width' => 145,
                        'type'        => 'text',
                        'tips'        => '请确认上方密码',
                        'default'     => '',
                        'required'    => true,
                    ],
                    [
                        'label'       => '站长邮箱',
                        'field'       => 'email',
                        'label-width' => 145,
                        'type'        => 'text',
                        'tips'        => '',
                        'default'     => 'admin@qq.com',
                        'required'    => true,
                    ],
                    // [
                    //     'label'       => '站长QQ',
                    //     'field'       => 'qq',
                    //     'label-width' => 145,
                    //     'type'        => 'text',
                    //     'tips'        => '',
                    //     'required'    => false,
                    // ],
                ],
            ],
        ],
    ],
];