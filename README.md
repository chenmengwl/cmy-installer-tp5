# cmy-installer-tp5

#### 介绍
适用于thinkphp5，thinkphp5.1使用的通用安装引导程序

#### 软件架构
深度兼容tp5和tp5.1框架，适度改改也可以用于tp6.x


#### 安装教程

1.  直接拷贝克隆下载zip资源包

#### 使用说明

1.  程序目录结构跟tp5完全一致，上传到对应的目录文件夹即可
2.  修改相应的安装和表单配置：在application/install/config.php
3.  修改配置需要检测的选项：在application/controller/Index.php中getCheckList方法下
4.  调整协议内容：在public/install/license.html中
5.  调整logo文件：在public/install/img/目录中替换相应logo文件即可

#### 相关截图

![输入图片说明](https://foruda.gitee.com/images/1689229289520829180/f6507756_6571151.png "1111.png")
![输入图片说明](https://foruda.gitee.com/images/1689229299615222156/56a1d176_6571151.png "2222.png")
![输入图片说明](https://foruda.gitee.com/images/1689229308207842492/6f6e5cb7_6571151.png "3333.png")

#### 参与贡献

1.  第一个版本由沉梦开发编译发布

#### 联系方式
微信：chenm857285711
QQ：857285711
欢迎加好友探讨交流更多php和前端技术
